poliastro (0.14.0-2) unstable; urgency=medium

  * Fix CI test
  * Explicitly add all dependencies to py3-poliastro

 -- Ole Streicher <olebole@debian.org>  Sun, 28 Jun 2020 15:37:47 +0200

poliastro (0.14.0-1) unstable; urgency=low

  * New upstream version 0.14.0. Rediff patches. Closes: #963391
  * Push Standards-Version to 4.5.0. No changes needed
  * Push dh-compat to 13
  * Fix some failing tests, and mark the remaining ones

 -- Ole Streicher <olebole@debian.org>  Mon, 18 May 2020 16:09:27 +0200

poliastro (0.13.1-1) unstable; urgency=low

  * Adjust minimal version of astropy. (Closes: #939462)
  * New upstream version 0.13.1
  * Drop patches that are applied upstream
  * Push Standards-Version to 4.4.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Fri, 20 Dec 2019 13:36:08 +0100

poliastro (0.13.0-1) unstable; urgency=low

  * New upstream version 0.13.0. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required
  * Push compat to 12. Remove d/compat
  * Add gitlab-ci.yml for salsa
  * Mark tests that use remote data
  * Skip failing poliastro.core doctests
  * Restrict build dep to plotly >=4

 -- Ole Streicher <olebole@debian.org>  Sun, 18 Aug 2019 14:59:05 +0200

poliastro (0.11.1-1) unstable; urgency=medium

  * New upstream version 0.11.1. Rediff patches
  * Push Standards-Version to 4.3.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Sat, 29 Dec 2018 11:38:04 +0100

poliastro (0.11.0-4) unstable; urgency=low

  * Define absolute tolerance when comparing with 0.0

 -- Ole Streicher <olebole@debian.org>  Mon, 08 Oct 2018 09:54:58 +0200

poliastro (0.11.0-3) unstable; urgency=low

  * Set MPLBACKEND for CI tests

 -- Ole Streicher <olebole@debian.org>  Fri, 28 Sep 2018 08:40:27 +0200

poliastro (0.11.0-2) unstable; urgency=low

  * Fix name of tests control file

 -- Ole Streicher <olebole@debian.org>  Thu, 27 Sep 2018 09:41:28 +0200

poliastro (0.11.0-1) unstable; urgency=low

  * Initial release. (Closes: #908659)

 -- Ole Streicher <olebole@debian.org>  Tue, 25 Sep 2018 11:56:50 +0200
